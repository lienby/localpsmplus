﻿namespace LocalPSM_
{

    partial class PSMKeyGen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PSMKeyGen));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.DevName = new System.Windows.Forms.TextBox();
            this.TextColor = new System.Windows.Forms.Label();
            this.KeyGen = new System.Windows.Forms.Button();
            this.LicenseGen = new System.Windows.Forms.Button();
            this.Unity = new System.Windows.Forms.CheckBox();
            this.ProjectName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::LocalPSM_.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(408, 131);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // DevName
            // 
            this.DevName.BackColor = System.Drawing.SystemColors.HotTrack;
            this.DevName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DevName.ForeColor = System.Drawing.SystemColors.Window;
            this.DevName.Location = new System.Drawing.Point(92, 175);
            this.DevName.MaxLength = 17;
            this.DevName.Name = "DevName";
            this.DevName.Size = new System.Drawing.Size(328, 20);
            this.DevName.TabIndex = 1;
            this.DevName.TextChanged += new System.EventHandler(this.DevName_TextChanged);
            // 
            // TextColor
            // 
            this.TextColor.AutoSize = true;
            this.TextColor.BackColor = System.Drawing.Color.Transparent;
            this.TextColor.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.TextColor.Location = new System.Drawing.Point(12, 175);
            this.TextColor.Name = "TextColor";
            this.TextColor.Size = new System.Drawing.Size(75, 13);
            this.TextColor.TabIndex = 2;
            this.TextColor.Text = "Device Name:";
            // 
            // KeyGen
            // 
            this.KeyGen.BackColor = System.Drawing.SystemColors.Highlight;
            this.KeyGen.Enabled = false;
            this.KeyGen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.KeyGen.ForeColor = System.Drawing.SystemColors.Window;
            this.KeyGen.Location = new System.Drawing.Point(12, 213);
            this.KeyGen.Name = "KeyGen";
            this.KeyGen.Size = new System.Drawing.Size(201, 26);
            this.KeyGen.TabIndex = 3;
            this.KeyGen.Text = "Generate Publishing License";
            this.KeyGen.UseVisualStyleBackColor = false;
            this.KeyGen.Click += new System.EventHandler(this.KeyGen_Click);
            // 
            // LicenseGen
            // 
            this.LicenseGen.BackColor = System.Drawing.SystemColors.Highlight;
            this.LicenseGen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LicenseGen.ForeColor = System.Drawing.SystemColors.Window;
            this.LicenseGen.Location = new System.Drawing.Point(219, 213);
            this.LicenseGen.Name = "LicenseGen";
            this.LicenseGen.Size = new System.Drawing.Size(201, 26);
            this.LicenseGen.TabIndex = 4;
            this.LicenseGen.Text = "Re-Activate License";
            this.LicenseGen.UseVisualStyleBackColor = false;
            this.LicenseGen.Click += new System.EventHandler(this.LicenseGen_Click);
            // 
            // Unity
            // 
            this.Unity.AutoSize = true;
            this.Unity.BackColor = System.Drawing.Color.Transparent;
            this.Unity.ForeColor = System.Drawing.SystemColors.Control;
            this.Unity.Location = new System.Drawing.Point(344, 151);
            this.Unity.Name = "Unity";
            this.Unity.Size = new System.Drawing.Size(76, 17);
            this.Unity.TabIndex = 5;
            this.Unity.Text = "PSM Unity";
            this.Unity.UseVisualStyleBackColor = false;
            this.Unity.CheckedChanged += new System.EventHandler(this.Unity_CheckedChanged);
            // 
            // ProjectName
            // 
            this.ProjectName.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ProjectName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProjectName.ForeColor = System.Drawing.SystemColors.Window;
            this.ProjectName.Location = new System.Drawing.Point(92, 149);
            this.ProjectName.MaxLength = 30;
            this.ProjectName.Name = "ProjectName";
            this.ProjectName.Size = new System.Drawing.Size(246, 20);
            this.ProjectName.TabIndex = 6;
            this.ProjectName.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(12, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Project Name:";
            // 
            // PSMKeyGen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LocalPSM_.Properties.Resources.bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(432, 262);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ProjectName);
            this.Controls.Add(this.Unity);
            this.Controls.Add(this.LicenseGen);
            this.Controls.Add(this.KeyGen);
            this.Controls.Add(this.TextColor);
            this.Controls.Add(this.DevName);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PSMKeyGen";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PSM Key Generator";
            this.Load += new System.EventHandler(this.PSMKeyGen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox DevName;
        private System.Windows.Forms.Label TextColor;
        private System.Windows.Forms.Button KeyGen;
        private System.Windows.Forms.Button LicenseGen;
        private System.Windows.Forms.CheckBox Unity;
        private System.Windows.Forms.TextBox ProjectName;
        private System.Windows.Forms.Label label1;
    }
}

